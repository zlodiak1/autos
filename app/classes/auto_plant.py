
class AutoPlant:
    def __init__(self, storage):
        self.cars = []
        self.storage = storage

    def construct_car(self, Car):
        if self.storage.engines and self.storage.wheels:
            self.engine = self.storage.engines[0]
            self.storage.delete_engine(self.storage.engines[0].serial_num)
            # print('---', self.engine, self.storage.engines)

            self.wheels = self.storage.wheels[0]
            self.storage.delete_wheels(self.storage.wheels[0].serial_num)
            # print('---', self.wheels, self.storage.wheels)

            self.cars.append(Car(self.engine, self.wheels))

            return self.cars
        else: 
            print('no construct')
