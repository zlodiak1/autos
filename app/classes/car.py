class FordCar:
    def __init__(self, engine, wheels):
        self.engine_serial_num = engine.serial_num
        self.engine_type = engine.type
        self.wheels_serial_num = wheels.serial_num
        self.wheels_type = wheels.type
        self.have_thorns = wheels.have_thorns

    def __repr__(self):
        return '<Ford: {}/{} : {}/{}/{}>'.format(
            self.engine_serial_num, 
            self.engine_type, 
            self.wheels_serial_num, 
            self.wheels_type,
            self.have_thorns
        )


class TeslaCar:
    def __init__(self, engine, wheels):
        self.engine_serial_num = engine.serial_num
        self.engine_type = engine.type
        self.wheels_serial_num = wheels.serial_num
        self.wheels_type = wheels.type
        self.have_thorns = wheels.have_thorns

    def __repr__(self):
        return '<Tesla: {}/{} : {}/{}/{}>'.format(
            self.engine_serial_num, 
            self.engine_type, 
            self.wheels_serial_num, 
            self.wheels_type,
            self.have_thorns
        )