import random


class EngineFabric:
    def __init__(self, type):
        self.type = type
        self.serial_num = random.randint(1000000, 9999999)

    def get_type(self):
        return self.type

    def get_serial_num(self):
        return self.serial_num

    def __repr__(self):
        return '<Engine_raw: {}/{}>'.format(self.type, self.serial_num)

