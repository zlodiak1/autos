class Storage:
    def __init__(self):
        self.engines = []
        self.wheels = []

    def load_engine(self, cnt, Engine, type):
        for i in range(cnt):
            engine = Engine(type)
            self.engines.append(engine)         

    def get_engines(self):
        return self.engines

    def delete_engine(self, serial_num):
        engines = self.engines
        result = []
        for engine in engines:
            if serial_num != engine.serial_num:
                result.append(engine)
        self.engines = result


    def load_wheels(self, cnt, Wheels, type, have_thorns):
        for i in range(cnt):
            wheels = Wheels(type, have_thorns)
            self.wheels.append(wheels)     

    def get_wheels(self):
        return self.wheels

    def delete_wheels(self, serial_num):
        wheels = self.wheels
        result = []
        for w in wheels:
            if serial_num != w.serial_num:
                result.append(w)
        self.wheels = result