import random


class WheelsFabric:
    def __init__(self, type, have_thorns):
        self.type = type
        self.serial_num = random.randint(1000000, 9999999)
        self.have_thorns = have_thorns

    def get_type(self):
        return self.type

    def get_serial_num(self):
        return self.serial_num

    def get_have_thorns(self):
        return self.have_thorns        

    def __repr__(self):
        return '<Wheels_raw: {}/{}/{}>'.format(
            self.type, 
            self.serial_num, 
            self.have_thorns
        )

