from app.classes.engine import *
from app.classes.wheels import *
from app.classes.storage import *
from app.classes.car import *
from app.classes.auto_plant import *


storage = Storage()


petrol_engine_cnt = input('сколько двигателей типа pertol загрузить на склад?')
electro_engine_cnt = input('сколько двигателей типа electro загрузить на склад?')

round_wheel_cnt = input('сколько комплектов колёс типа round загрузить на склад?')
square_wheel_cnt = input('сколько комплектов колёс типа squaare загрузить на склад?')


storage.load_engine(int(petrol_engine_cnt), EngineFabric, 'petrol')
storage.load_engine(int(electro_engine_cnt), EngineFabric, 'electro')
# print(storage.get_engines())

storage.load_wheels(int(round_wheel_cnt), WheelsFabric, 'round', False)
storage.load_wheels(int(square_wheel_cnt), WheelsFabric, 'square', True)
# print(storage.get_wheels())


auto_plant = AutoPlant(storage)
auto_plant.construct_car(FordCar)
auto_plant.construct_car(TeslaCar)
print(auto_plant.cars)
